import { Meteor } from 'meteor/meteor';
import { Webpages } from '../imports/api/webpages.js';

import cloudinary from 'cloudinary';
import request from 'request';
import { URL } from 'url';

const { cloud_name, api_key, api_secret } = Meteor.settings.cloudinary;
cloudinary.config({
	cloud_name,
	api_key,
	api_secret
});
Meteor.startup(() => {
	console.log(new URL('http://etf.unsa.ba/').host.split('.')[1]);
	Meteor.methods({
		storeImage: function(url) {
			return new Promise((resolve, reject) => {
				request(
					{
						url: 'https://api.apiflash.com/v1/urltoimage',
						encoding: null,
						qs: {
							url,
							full_page: true,
							access_key: Meteor.settings.apiflash.access_key
						}
					},
					Meteor.bindEnvironment(function(error, response, body) {
						if (error) {
							reject(new Meteor.Error('Api Flash Error', error));
						} else {
							cloudinary.v2.uploader
								.upload_stream(
									{},
									Meteor.bindEnvironment((error, result) => {
										if (error) {
											reject(new Meteor.Error('Cloudinary Error', error));
										} else {
											return Meteor.call(
												'webpages.insert',
												{
													title: new URL(url).host.split('.')[1],
													url,
													imageUrl: result.url
												},
												(error, result) =>
													error
														? reject(
																new Meteor.Error(
																	'Error while inserting into collection',
																	error
																)
														  )
														: resolve(result)
											);
										}
									})
								)
								.end(new Buffer(body));
						}
					})
				);
			});
		}
	});
});
