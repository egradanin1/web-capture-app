import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const Webpages = new Mongo.Collection('webpages');

if (Meteor.isServer) {
	Meteor.publish('webpages', function webpagesPublication() {
		return Webpages.find({});
	});
}

Meteor.methods({
	'webpages.insert'(webpage) {
		let { title, url, imageUrl } = webpage;
		check(title, String);
		check(url, String);
		check(imageUrl, String);

		Webpages.insert({
			title,
			url,
			imageUrl
		});
	}
});
