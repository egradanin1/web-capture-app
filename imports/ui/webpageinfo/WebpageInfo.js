import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';

import './WebpageInfo.css';

import { withTracker } from 'meteor/react-meteor-data';
import { Webpages } from '../../api/webpages.js';

class WebpageInfo extends Component {
	render() {
		if (this.props.isLoading) return <div>Loading!!!</div>;
		let webpage = this.props.webpage;
		return (
			<div>
				<div className="details">
					<div className="title">
						<h3>{webpage.title}</h3>
						<h4>
							<a href={webpage.url}>({webpage.url})</a>
						</h4>
					</div>
					<img src={webpage.imageUrl} alt={webpage.title} />
					<Link to="/">
						<div className="goback" />
					</Link>
				</div>
			</div>
		);
	}
}

export default withTracker(props => {
	let subscription = Meteor.subscribe('webpages');
	let isLoading = !subscription.ready();
	let webpageId = props.match.params.id;

	return {
		webpage: Webpages.findOne(webpageId),
		isLoading
	};
})(WebpageInfo);
