import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Link, Redirect } from 'react-router-dom';

import WebpageCard from '../webpagecard/WebpageCard.js';
import './Dashboard.css';

import { withTracker } from 'meteor/react-meteor-data';
import { Webpages } from '../../api/webpages.js';
import ReactLoading from 'react-loading';

class Dashboard extends Component {
	state = {
		url: '',
		isCaptureLoading: false
	};
	handleInput(e) {
		this.setState({
			url: e.target.value
		});
	}
	validateUrl(url) {
		var expression = /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi;
		var regex = new RegExp(expression);
		return url.match(regex);
	}
	handleClick() {
		let { url } = this.state;
		if (!this.validateUrl(url)) alert('Please provide a link that starts with http or https');
		else {
			this.setState({ isCaptureLoading: true });
			Meteor.call('storeImage', url, (errorClass, result) => {
				this.setState({ isCaptureLoading: false, url: '' });
				if (errorClass) {
					alert(errorClass.error);
				}
			});
		}
	}

	renderWebpages() {
		let webpages = this.props.webpages;
		return webpages.map((webpage, index) => {
			return <WebpageCard {...webpage} key={webpage._id} id={index} />;
		});
	}

	render() {
		let { isLoading } = this.props;
		let { url, isCaptureLoading } = this.state;
		if (this.props.isLoading) return <div>Loading!!!</div>;
		return (
			<div>
				<input
					type="text"
					value={url}
					id="capture-input"
					onChange={e => this.handleInput(e)}
					placeholder="Capture Website"
				/>
				<button className="capture-button" onClick={() => this.handleClick()}>
					Capture
				</button>
				<ReactLoading
					hidden={!isCaptureLoading}
					type="spinningBubbles"
					color="#b72744"
					height={200}
					width={200}
					className="spin"
				/>
				<div className="webpage-list">{this.renderWebpages()}</div>
			</div>
		);
	}
}

export default withTracker(() => {
	let subscriptionWebpages = Meteor.subscribe('webpages');
	let isLoading = !subscriptionWebpages.ready();

	return {
		webpages: Webpages.find().fetch(),
		isLoading
	};
})(Dashboard);
