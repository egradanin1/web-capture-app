import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import './WebpageCard.css';

class WebpageCard extends Component {
	render() {
		return (
			<div className="webpage-item">
				<Link to={`/details/${this.props._id}`}>
					<img alt={this.props.title} src={this.props.imageUrl} />
					<div className="basic-info">
						<h3>{this.props.title}</h3>
						<h4>({this.props.url})</h4>
					</div>
				</Link>
			</div>
		);
	}
}

export default WebpageCard;
