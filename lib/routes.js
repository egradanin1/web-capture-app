import React from 'react';
import { Router, Route, Switch } from 'react-router';

import createBrowserHistory from 'history/createBrowserHistory';

// route components
import AppContainer from '../imports/ui/appcontainer/AppContainer.js';
import Dashboard from '../imports/ui/dashboard/Dashboard.js';
import WebpageInfo from '../imports/ui/webpageinfo/WebpageInfo.js';

const browserHistory = createBrowserHistory();

export default (
	<Router history={browserHistory}>
		<AppContainer>
			<Route exact={true} path="/" component={Dashboard} />
			<Route path="/details/:id" component={WebpageInfo} />
		</AppContainer>
	</Router>
);
