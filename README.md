# web-capture-app

A meteor sample app to demonstrate how a web app can be built using a Meteor framework and mongo database with React.

### Building and starting the app

To build the app and start meteor, navigate to the web-capture-app directory and run the following commands:

    meteor npm install
    meteor --settings settings.json

After that the app starts and it will be accessible on port 3000. Following APIs were used during the implementation of this app:

-   Cloudinary
-   ApiFlash
